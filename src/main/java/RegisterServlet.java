import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discover = req.getParameter("discover");
		String dateOfBirth = req.getParameter("date_of_birth");
		String typeOfWork = req.getParameter("type_of_work");
		String description = req.getParameter("description");
		
		//Stores all the data from the form into the session
		HttpSession session = req.getSession();
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discover", discover);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("typeOfWork", typeOfWork);
		session.setAttribute("description", description);
		
		res.sendRedirect("confirmation.jsp");

		
	}
	
	public void destroy () {
		System.out.println("RegisterServlet has been destroyed");
	}
	
}
